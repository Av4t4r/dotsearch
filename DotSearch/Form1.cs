﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Threading;

namespace DotSearch
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        delegate void ObjectDelegate (object obj); //this delegate lets the GUI update.
        Thread th;                                 //this thread will perform the search
        public string filename;

        private void searchBtn_Click (Object sender, EventArgs e)
        {
            try
            {
                if ((filenameTextBox.Text != "") & (directoryTextBox.Text != ""))  //we check that there is a filename and a directory selected
                {
                    searchBtn.Enabled = false;                          //disable this button to prevent the execution of like a billion of search threads
                    resultsListBox.Items.Clear();                       //we clear the previous results (if there were any)
                    directoryTextBox.Text.Replace("\\", "\\\"");        //this is to properly parse subdirectories
                    object path = directoryTextBox.Text;                //we save the directory as an object to send it as a parameter to the thread
                    filename = filenameTextBox.Text + "*";              //The wildcard "*" at the end, allows the user to find every filename that BEGINS with what he/she types
                    if (subdirectoriesCheckBox.Checked == true)         //checking wether to search in subdirectories or not...
                    {
                        ObjectDelegate del = new ObjectDelegate(fileSearch);    //We instance the delegate, adding the fileSearch method to it
                        del += subSearch;                                       //And we are also adding subSearch, 'cause of the subdirectories
                        del += searchDone;
                        th = new Thread(new ParameterizedThreadStart(del));     //The delegate is being started in a separate Thread...
                        th.Start(path);                                         //...sending the directory as a parameter to all of them

                    }
                    else
                    {
                        ObjectDelegate del = new ObjectDelegate(fileSearch);
                        del += searchDone;
                        th = new Thread(new ParameterizedThreadStart(del));
                        th.Start(path);
                    }
                }
                else
                {MessageBox.Show("No filename or directory selected!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);}
            }
            catch { }
        }



        #region NewSubdirectorySearch

        private void subSearch (object pathArg)
        {
            string path = pathArg.ToString();
            try
            {
                foreach (string sd in Directory.GetDirectories(path))
                {
                    try
                    {
                        foreach (string files in Directory.GetFiles(sd, filename))
                        {
                            FillResults(files);
                        }
                    }
                    catch (System.UnauthorizedAccessException)
                    {
                        subSearch(sd);
                    }
                    subSearch(sd);
                }
            }
            catch{}

        }

        private void fileSearch (object pathArg)
        {
            string path = pathArg.ToString();
                try
                {
                    foreach (string fn in Directory.GetFiles(path, filename))
                    {
                        FillResults(fn);
                    }
                }
                catch (System.UnauthorizedAccessException) { }
        }

        private void FillResults (object results)
        {
            if (InvokeRequired)
            {
                ObjectDelegate method = new ObjectDelegate(FillResults);
                Invoke(method, results);
                return;
            }
            string text = results.ToString();
            resultsListBox.Items.Add(text);
            return;
        }

        private void searchDone (object obj)
        {
            if (InvokeRequired)
            {
                ObjectDelegate method = new ObjectDelegate(searchDone);
                Invoke(method, obj);
                return;
            }
            searchBtn.Enabled = true;
            if (resultsListBox.Items.Count == 0)
            {resultsListBox.Items.Add("No results");}
            th.Abort();
        }
        #endregion

        #region EverythingElse
        private void directoryBtn_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog carpetas = new FolderBrowserDialog();
            carpetas.ShowNewFolderButton = false;            
            DialogResult carpetasResult = carpetas.ShowDialog();
            if (carpetasResult == DialogResult.OK)
            {
                directoryTextBox.Text = carpetas.SelectedPath;
            }
            else{}
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.Text = ProductName + " v" + ProductVersion;
            resultsListBox.ContextMenuStrip = resultContextMenu;
        }

        private void resultsListBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                Process.Start("explorer.exe", resultsListBox.SelectedItem.ToString());
            }
            catch { }
            
        }

        private void resultsListBox_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    resultsListBox.SelectedIndex = resultsListBox.IndexFromPoint(e.Location);
                }
                resultContextMenu.Show();
            }
            catch { }
        }

        private void openDirectoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("explorer.exe", "/select," + '"' + resultsListBox.SelectedItem.ToString() + '"');
            }
            catch (System.NullReferenceException) { MessageBox.Show("No file selected!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }
        #endregion

        private void MainForm_FormClosing (object sender, FormClosingEventArgs e)
        {
            try { th.Abort(); }
            catch { }
        }

        private void stopBtn_Click (object sender, EventArgs e)
        {
            try 
            {
                if (th.IsAlive)
                {
                    th.Abort();
                    searchDone(sender);
                }
                else
                { MessageBox.Show("There's no search to stop!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
            catch
            { MessageBox.Show("There's no search to stop!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }
    }
}
