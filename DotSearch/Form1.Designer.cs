﻿namespace DotSearch
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.searchBtn = new System.Windows.Forms.Button();
            this.resultsListBox = new System.Windows.Forms.ListBox();
            this.filenameTextBox = new System.Windows.Forms.TextBox();
            this.filenameLabel = new System.Windows.Forms.Label();
            this.directorySearchLabel = new System.Windows.Forms.Label();
            this.directoryBtn = new System.Windows.Forms.Button();
            this.directoryTextBox = new System.Windows.Forms.TextBox();
            this.subdirectoriesCheckBox = new System.Windows.Forms.CheckBox();
            this.resultContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.openDirectoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopBtn = new System.Windows.Forms.Button();
            this.resultContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // searchBtn
            // 
            this.searchBtn.Location = new System.Drawing.Point(12, 329);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(126, 23);
            this.searchBtn.TabIndex = 0;
            this.searchBtn.Text = "Find it!";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // resultsListBox
            // 
            this.resultsListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultsListBox.FormattingEnabled = true;
            this.resultsListBox.HorizontalScrollbar = true;
            this.resultsListBox.Location = new System.Drawing.Point(168, 0);
            this.resultsListBox.Name = "resultsListBox";
            this.resultsListBox.Size = new System.Drawing.Size(777, 407);
            this.resultsListBox.TabIndex = 1;
            this.resultsListBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.resultsListBox_MouseDoubleClick);
            // 
            // filenameTextBox
            // 
            this.filenameTextBox.Location = new System.Drawing.Point(12, 25);
            this.filenameTextBox.Name = "filenameTextBox";
            this.filenameTextBox.Size = new System.Drawing.Size(143, 20);
            this.filenameTextBox.TabIndex = 2;
            // 
            // filenameLabel
            // 
            this.filenameLabel.AutoSize = true;
            this.filenameLabel.Location = new System.Drawing.Point(12, 9);
            this.filenameLabel.Name = "filenameLabel";
            this.filenameLabel.Size = new System.Drawing.Size(95, 13);
            this.filenameLabel.TabIndex = 3;
            this.filenameLabel.Text = "Write the filename:";
            // 
            // directorySearchLabel
            // 
            this.directorySearchLabel.AutoSize = true;
            this.directorySearchLabel.Location = new System.Drawing.Point(12, 48);
            this.directorySearchLabel.Name = "directorySearchLabel";
            this.directorySearchLabel.Size = new System.Drawing.Size(89, 13);
            this.directorySearchLabel.TabIndex = 5;
            this.directorySearchLabel.Text = "Choose directory:";
            // 
            // directoryBtn
            // 
            this.directoryBtn.Location = new System.Drawing.Point(12, 64);
            this.directoryBtn.Name = "directoryBtn";
            this.directoryBtn.Size = new System.Drawing.Size(143, 23);
            this.directoryBtn.TabIndex = 6;
            this.directoryBtn.Text = "ChooseDirectory";
            this.directoryBtn.UseVisualStyleBackColor = true;
            this.directoryBtn.Click += new System.EventHandler(this.directoryBtn_Click);
            // 
            // directoryTextBox
            // 
            this.directoryTextBox.Location = new System.Drawing.Point(12, 93);
            this.directoryTextBox.Name = "directoryTextBox";
            this.directoryTextBox.Size = new System.Drawing.Size(143, 20);
            this.directoryTextBox.TabIndex = 7;
            // 
            // subdirectoriesCheckBox
            // 
            this.subdirectoriesCheckBox.AutoSize = true;
            this.subdirectoriesCheckBox.Checked = true;
            this.subdirectoriesCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.subdirectoriesCheckBox.Location = new System.Drawing.Point(15, 306);
            this.subdirectoriesCheckBox.Name = "subdirectoriesCheckBox";
            this.subdirectoriesCheckBox.Size = new System.Drawing.Size(99, 17);
            this.subdirectoriesCheckBox.TabIndex = 8;
            this.subdirectoriesCheckBox.Text = "Subdirectories?";
            this.subdirectoriesCheckBox.UseVisualStyleBackColor = true;
            // 
            // resultContextMenu
            // 
            this.resultContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openDirectoryToolStripMenuItem});
            this.resultContextMenu.Name = "resultContextMenu";
            this.resultContextMenu.Size = new System.Drawing.Size(155, 26);
            // 
            // openDirectoryToolStripMenuItem
            // 
            this.openDirectoryToolStripMenuItem.Name = "openDirectoryToolStripMenuItem";
            this.openDirectoryToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.openDirectoryToolStripMenuItem.Text = "Open Directory";
            this.openDirectoryToolStripMenuItem.Click += new System.EventHandler(this.openDirectoryToolStripMenuItem_Click);
            // 
            // stopBtn
            // 
            this.stopBtn.Location = new System.Drawing.Point(12, 359);
            this.stopBtn.Name = "stopBtn";
            this.stopBtn.Size = new System.Drawing.Size(126, 23);
            this.stopBtn.TabIndex = 10;
            this.stopBtn.Text = "Stop searching!";
            this.stopBtn.UseVisualStyleBackColor = true;
            this.stopBtn.Click += new System.EventHandler(this.stopBtn_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(938, 411);
            this.Controls.Add(this.stopBtn);
            this.Controls.Add(this.subdirectoriesCheckBox);
            this.Controls.Add(this.directoryTextBox);
            this.Controls.Add(this.directoryBtn);
            this.Controls.Add(this.directorySearchLabel);
            this.Controls.Add(this.filenameLabel);
            this.Controls.Add(this.filenameTextBox);
            this.Controls.Add(this.resultsListBox);
            this.Controls.Add(this.searchBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(181, 432);
            this.Name = "MainForm";
            this.Text = "DotSearch";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.resultContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.ListBox resultsListBox;
        private System.Windows.Forms.TextBox filenameTextBox;
        private System.Windows.Forms.Label filenameLabel;
        private System.Windows.Forms.Label directorySearchLabel;
        private System.Windows.Forms.Button directoryBtn;
        private System.Windows.Forms.CheckBox subdirectoriesCheckBox;
        private System.Windows.Forms.ContextMenuStrip resultContextMenu;
        private System.Windows.Forms.ToolStripMenuItem openDirectoryToolStripMenuItem;
        private System.Windows.Forms.TextBox directoryTextBox;
        private System.Windows.Forms.Button stopBtn;
    }
}

